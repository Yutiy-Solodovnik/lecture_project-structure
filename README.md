## Requirements:
Create an ASP.NET Core WebAPI application with one of the following architectures:   
REST API (controllers / services) or CQRS   
Implement Create / Read / Update / Delete for all entities + LINQ homework queries    
In the console application from the previous task, change the endpoint address so that now the client is knocking on your new API   
Use Dependency Injection   
Use DTOs for Data Types in Queries   
## Notes:   
To work with data, use one of the patterns discussed in the video (Repository, UnitOfWork).   
Store data in memory (you DO NOT need to use the database!). One can simply store List of objects in repositories.    