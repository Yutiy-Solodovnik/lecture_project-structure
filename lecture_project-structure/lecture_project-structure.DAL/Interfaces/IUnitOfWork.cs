﻿using lecture_project_structure.DAL.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<MyTask> Tasks { get; }
        IRepository<Project> Projects { get; }
        IRepository<Team> Teams { get; }
        IRepository<User> Users { get; }
    }
}
