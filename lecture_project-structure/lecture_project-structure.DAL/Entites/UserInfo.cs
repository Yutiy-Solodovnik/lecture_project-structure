﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.DAL.Entites
{
    public class UserInfo
    {
        public User user { get; set; }
        public Project lastProject { get; set; }
         
        public int allTasks { get; set; }
        public int allUnfinishedTasks { get; set; }
         
        public MyTask longestTask { get; set; }
    }
}
