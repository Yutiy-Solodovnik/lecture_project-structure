﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.DAL.Entites
{
    public class Project
    {
        public int id { get; set; }
        public int authorId { get; set; }
        public int teamId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime deadline { get; set; }
        public DateTime createdAt { get; set; }
        public List<MyTask> tasks { get; set; }
        public Team team { get; set; }
        public User author { get; set; }
        public override string ToString()
        {
            return $"{name}|Автор: {author}|Команда: {team}|Описание: {description}|Создан: {createdAt}|Дедлайн: {deadline}";
        }
    }
}
