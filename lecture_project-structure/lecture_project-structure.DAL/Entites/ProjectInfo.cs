﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.DAL.Entites
{
    public class ProjectInfo
    { 
        public Project project { get; set; }
        public MyTask longestTask { get; set; }
        public MyTask shortestTask { get; set; }
        public int allUsersOnProject { get; set; }
    }
}
