﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.DAL.Entites
{
    public class User
    {
        public int id { get; set; }
        public int? teamId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public DateTime registeredAt { get; set; }
        public DateTime birthDay { get; set; }
        public override string ToString()
        {
            return $"{firstName}|{lastName}|Почта: {email}|Зарегестрирован: {registeredAt}|День рождения: {birthDay}";
        }
    }
}
