﻿using lecture_project_structure.DAL.Entites;
using lecture_project_structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private MyTaskRepositry _myTaskRepository;
        private ProjectRepository _projectRepository = new ProjectRepository();
        private TeamRepository _teamRepository;
        private UserRepository _userRepository;
        public IRepository<MyTask> Tasks
        {
            get
            {
                if (_myTaskRepository == null)
                    _myTaskRepository = new MyTaskRepositry();
                return _myTaskRepository;
            }
        }
        public IRepository<Project> Projects
        {
            get
            {
               
                return _projectRepository;
            }
        }
        public IRepository<Team> Teams
        {
            get
            {
                if (_teamRepository == null)
                    _teamRepository = new TeamRepository();
                return _teamRepository;
            }
        }
        public IRepository<User> Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository();
                return _userRepository;
            }
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
