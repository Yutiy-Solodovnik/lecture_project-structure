﻿using lecture_project_structure.BLL.DTO;
using lecture_project_structure.DAL.Entites;
using System;

namespace lecture_project_structure.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            string stringVariant;
            Update();
            do
            {
                Console.WriteLine();
                Console.WriteLine("\nВыберите действие:\n1. Получить кол-во тасков у проекта конкретного пользователя (по id)" +
                " (словарь, где ключом будет проект, а значением кол-во тасков).\n2. Получить список тасков, назначенных на " +
                "конкретного пользователя (по id), где name таска < 45 символов (коллекция из тасков). \n3. Получить список (id, name) " +
                "из коллекции тасков, которые выполнены (finished) в текущем (2021) году для конкретного пользователя (по id). " +
                "\n4. Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет," +
                " отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам. " +
                "\n5. Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию)." +
                "\n6. Получить следующую структуру (передать Id пользователя в параметры):\n-User\n-Последний проект пользователя(по дате создания)" +
                "\n-Общее кол - во тасков под последним проектом" +
                "\n-Общее кол - во незавершенных или отмененных тасков для пользователя \n-Самый долгий таск пользователя по дате" +
                "\n7. Получить следующую структуру: \n-Проект \n-Самый длинный таск проекта(по описанию) \n-Самый короткий таск проекта(по имени) " +
                "\n-Общее кол - во пользователей в команде проекта, где или описание проекта > 20 символов или кол - во тасков < 3 \n 8. Обновить структуру\n 0. Выход");
                Console.WriteLine();
                Validate(out stringVariant, 0, 10);
                switch (Byte.Parse(stringVariant))
                {
                    case 1:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (Byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    Console.WriteLine("Введите id");
                                    foreach (var i in JsonReader.GetTasksInProject(Int32.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine(i.Key.ToString() + " Количество тасков " + i.Value);
                                    }
                                    break;
                                case 1:
                                    Console.WriteLine("Введите id");
                                    foreach (var i in QueryBuilder.GetTasksInProject(Int32.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine(i.Key.ToString() + " Количество тасков " + i.Value);
                                    }
                                    break;
                            }
                            
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 2:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (Byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    Console.WriteLine("Введите id");

                                    foreach (var i in JsonReader.GetTasksByUser(Int32.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine(i.id + " " + i.name);
                                    }
                                    break;
                                case 1:
                                    Console.WriteLine("Введите id");

                                    foreach (var i in QueryBuilder.GetTasksByUser(Int32.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine(i.ToString());
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 3:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (Byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    Console.WriteLine("Введите id");

                                    foreach (var i in JsonReader.GetTasksByUser(Int32.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine(i.id +" "+ i.name);
                                    }
                                    break;
                                case 1:
                                    Console.WriteLine("Введите id");

                                    foreach (var i in QueryBuilder.GetFinishedTasksByUser(Int32.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine($"ID:{i.id}, имя: {i.name}");
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 4:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (Byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    Console.WriteLine("Введите id");

                                    foreach (var i in JsonReader.GetFinishedTasksByUser(Int32.Parse(Console.ReadLine())))
                                    {
                                        Console.WriteLine($"ID:{i.id}, имя: {i.name}");
                                    }
                                    break;
                                case 1:
                                    foreach (var i in QueryBuilder.GetUsersFromTeam())
                                    {
                                        Console.WriteLine("Id команды: " + i.Key);
                                        foreach (var user in i)
                                            Console.WriteLine(user.ToString());
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 5:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (Byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    foreach (var i in JsonReader.GetUsersByName())
                                    {
                                        Console.WriteLine(i.ToString());
                                    }
                                    break;
                                case 1:
                                    foreach (var i in JsonReader.GetUsersByName())
                                    {
                                        Console.WriteLine(i.ToString());
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 6:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (Byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    Console.WriteLine("Введите id");
                                    UserInfoDto userInfo0 = JsonReader.GetUserInfo(Int32.Parse(Console.ReadLine()));
                                    Console.WriteLine($"{userInfo0.userId} {userInfo0.lastProjectId} {userInfo0.allTasks} {userInfo0.allUnfinishedTasks} {userInfo0.longestTaskId}");
                                    break;
                                case 1:
                                    Console.WriteLine("Введите id");
                                    UserInfo userInfo = QueryBuilder.GetUserInfo(Int32.Parse(Console.ReadLine()));
                                    Console.WriteLine($"{userInfo.user} {userInfo.lastProject} {userInfo.allTasks} {userInfo.allUnfinishedTasks} {userInfo.longestTask}");
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 7:
                        try
                        {
                            Console.WriteLine("0 - Взять данные с сервера, 1 - Использовать локальный Query BUilder");
                            string stringWayVariant;
                            Validate(out stringWayVariant, 0, 1);
                            switch (Byte.Parse(stringWayVariant))
                            {
                                case 0:
                                    foreach (var i in JsonReader.GetProjectInfo())
                                    {
                                        Console.WriteLine($"{i.projectId} | Самый длинный таск {i.longestTaskId} |" +
                                            $"Самый короткий таск {i.shortestTaskId} | Количество пользователей {i.allUsersOnProject}");
                                    }
                                    break;
                                case 1:
                                    foreach (var i in QueryBuilder.GetProjectInfo())
                                    {
                                        Console.WriteLine($"{i.project.ToString()} | Самый длинный таск {i.longestTask?.ToString()} |" +
                                            $"Самый короткий таск {i.shortestTask?.ToString()} | Количество пользователей {i.allUsersOnProject}");
                                    }
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                    case 8:
                        try
                        {
                            Update();
                        }
                        catch (Exception ex)
                        {
                            WriteWrongMessage(ex.Message);
                        }
                        break;
                }
            } while (Byte.Parse(stringVariant) != 0);

            static void Validate(out string stringVariant, int from, int to)
            {
                do
                {
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.Green;
                    stringVariant = Console.ReadLine();
                    Console.ForegroundColor = ConsoleColor.White;
                    if (!IsRightInDiapason(stringVariant, from, to))
                    {
                        WriteWrongMessage("Ошибка! Неверный ввод");
                    }
                }
                while (!IsRightInDiapason(stringVariant, from, to));
            }

            static bool IsRightInDiapason(string stringVariant, int from, int to)
            {
                int variant;

                if (Int32.TryParse(stringVariant, out variant))
                {
                    if (variant >= from && variant <= to)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }

            static void WriteWrongMessage(string message)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.White;
            }

            static void Update()
            {
                Console.WriteLine("Выполняется считывание данных...");
                QueryBuilder.GetHierarchy();
            }
        }
    }
}
