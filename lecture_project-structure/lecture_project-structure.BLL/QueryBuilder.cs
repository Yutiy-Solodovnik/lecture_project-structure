﻿using System;
using lecture_project_structure.DAL.Entites;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lecture_project_structure.DAL.Interfaces;
using lecture_project_structure.DAL.Repositories;

namespace lecture_project_structure.BLL
{
    public static class QueryBuilder
    {
        private static List<Project> _projectsList;
        private static List<User> _usersList;
        private static List<Team> _teamsList;
        private static List<MyTask> _tasksList;
        private static List<Project> _hierarchy;
        private static IUnitOfWork unitOfWork;

        static QueryBuilder()
        {
            unitOfWork = new UnitOfWork();
            GetHierarchy();
        }
        public static void UpdateInformation()
        {
            _projectsList = unitOfWork.Projects.ReadAll().ToList();
            _usersList = unitOfWork.Users.ReadAll().ToList();
            _teamsList = unitOfWork.Teams.ReadAll().ToList();
            _tasksList = unitOfWork.Tasks.ReadAll().ToList();
        }

        public static void GetHierarchy()
        {
            if (_hierarchy == null)
                UpdateInformation();
            _hierarchy = _projectsList.GroupJoin(_tasksList, p => p.id, t => t.projectId,
                (p, t) => new Project()
                {
                    id = p.id,
                    authorId = p.authorId,
                    teamId = p.teamId,
                    name = p.name,
                    description = p.description,
                    deadline = p.deadline,
                    createdAt = p.createdAt,
                    tasks = t.GroupJoin(_usersList, t => t.performerId, u => u.id,
                        (t, u) => new MyTask()
                        {
                            id = t.id,
                            projectId = t.projectId,
                            performerId = t.performerId,
                            performer = _usersList.Where(u => u.id == t.performerId).FirstOrDefault(),
                            name = t.name,
                            description = t.description,
                            state = t.state,
                            createdAt = t.createdAt,
                            finishedAt = t.finishedAt
                        }
                        ).ToList(),
                    team = _teamsList.Where(t => t.id == p.teamId).FirstOrDefault(),
                    author = _usersList.Where(u => u.id == p.authorId).First()
                })
                .ToList();
        }

        public static Dictionary<string, int> GetTasksInProject(int id)
        {
            return _hierarchy
                .Where(u => u.authorId == id)
                .Select(t => new { projectName = t.name, taskCunt = t.tasks.Count() })
                .ToDictionary(x => x.projectName, x => x.taskCunt);
        }

        public static List<MyTask> GetTasksByUser(int id)
        {
            return _hierarchy
               .SelectMany(t => t.tasks)
               .Where(t => t.performerId == id && t.name.Length < 45)
               .ToList();
        }
        public static List<MyTaskInfo> GetFinishedTasksByUser(int id)
        {
            return _hierarchy
               .SelectMany(t => t.tasks)
               .Where(t => t.performerId == id && t.finishedAt != null)
               .Select(t => new MyTaskInfo { id = t.id, name = t.name }).ToList();
        }

        public static IEnumerable<IGrouping<int?, User>> GetUsersFromTeam()
        {
            return _hierarchy
               .SelectMany(t => t.tasks)
               .Select(u => u.performer)
               .Where(u => (DateTime.Now.Year - u.birthDay.Year) > 10)
               .Distinct().OrderBy(y => y.registeredAt)
               .GroupBy(t => t.teamId);
        }

        public static List<User> GetUsersByName()
        {
            return _hierarchy
               .SelectMany(t => t.tasks)
               .OrderByDescending(t => t.name)
               .Select(u => u.performer)
               .Distinct()
               .OrderBy(y => y.firstName)
               .ToList();
        }

        public static UserInfo GetUserInfo(int id)
        {
            return _hierarchy
               .Select(p => new UserInfo
               {
                   user = _hierarchy
                           .SelectMany(t => t.tasks)
                           .Select(u => u.performer)
                           .Where(u => u.id == id)
                           .Select(u => u)
                           .First(),
                   lastProject = _hierarchy
                            .Where(u => u.authorId == id)
                            .OrderBy(x => x.createdAt)
                            .First(),
                   allTasks = _hierarchy
                            .Where(u => u.authorId == id)
                            .OrderBy(x => x.createdAt)
                            .First().tasks
                            .Count(),
                   allUnfinishedTasks = _hierarchy
                           .SelectMany(t => t.tasks)
                           .Where(t => t.performerId == id && t.finishedAt == null)
                           .Count(),
                   longestTask = _hierarchy
                           .SelectMany(t => t.tasks)
                           .Where(t => t.performerId == id)
                           .OrderByDescending(x => (x.finishedAt ?? DateTime.Now) - x.createdAt)
                           .First()
               }).ToList()[0];
        }

        public static List<ProjectInfo> GetProjectInfo()
        {
            return _hierarchy
               .Select(p => new ProjectInfo
               {
                   project = p,
                   longestTask = p.tasks
                       .OrderByDescending(x => x.description.Length)
                       .FirstOrDefault(),
                   shortestTask = p.tasks
                        .OrderBy(x => x.name.Length)
                        .FirstOrDefault(),
                   allUsersOnProject = p.tasks
                        .Where(t => p.description.Length > 20 || p.tasks.Count() < 3)
                        .Select(t => t.performer)
                        .Count()
               }).ToList();
        }
    }
}
