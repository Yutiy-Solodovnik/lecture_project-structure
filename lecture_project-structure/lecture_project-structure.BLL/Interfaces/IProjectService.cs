﻿using lecture_project_structure.BLL.DTO;
using lecture_project_structure.DAL.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.Interfaces
{
    public interface IProjectService
    {
        void CreateProject(Project project);
        IEnumerable<Project> GetAllProjects();
        Project GetProject(int id);
        void UpdateProject(Project project);
        void DeleteProject(int id);
        void Dispose();
    }
}
