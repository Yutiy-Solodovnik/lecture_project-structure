﻿using lecture_project_structure.DAL.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.Interfaces
{
    public interface IMyTaskService
    {
        void CreateTask(MyTask task);
        IEnumerable<MyTask> GetAllTasks();
        MyTask GetTask(int id);
        void UpdateTask(MyTask task);
        void DeleteTask(int id);
        void Dispose();
    }
}
