﻿using lecture_project_structure.DAL.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.Interfaces
{
    public interface ITeamService
    {
        void CreateTeam(Team team);
        IEnumerable<Team> GetAllTeams();
        Team GetTeam(int id);
        void UpdateTeam(Team team);
        void DeleteTeam(int id);
        void Dispose();
    }
}
