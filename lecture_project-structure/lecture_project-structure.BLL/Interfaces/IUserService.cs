﻿using lecture_project_structure.DAL.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.Interfaces
{
    public interface IUserService
    {
        void CreateUser(User team);
        IEnumerable<User> GetAllUsers();
        User GetUser(int id);
        void UpdateUser(User user);
        void DeleteUser(int id);
        void Dispose();
    }
}
