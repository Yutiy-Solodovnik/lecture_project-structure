﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.DTO
{
    public class ProjectDTO
    {
         public int id { get; set; }
         public int authorId { get; set; }
         public int teamId { get; set; }
         public string name { get; set; }
         public string description { get; set; }
         public DateTime deadline { get; set; }
         public DateTime createdAt { get; set; }
    }
}
