﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.DTO
{
    public class MyTaskInfoDTO
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
