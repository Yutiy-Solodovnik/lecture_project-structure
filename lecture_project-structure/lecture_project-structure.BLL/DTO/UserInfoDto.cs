﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.DTO
{
    public class UserInfoDto
    {
        public int userId { get; set; }
        public int lastProjectId { get; set; }

        public int allTasks { get; set; }
        public int allUnfinishedTasks { get; set; }

        public int longestTaskId { get; set; }
    }
}
