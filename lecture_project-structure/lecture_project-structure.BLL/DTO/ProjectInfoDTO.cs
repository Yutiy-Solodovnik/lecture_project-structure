﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.DTO
{
    public class ProjectInfoDTO
    {
        public int projectId { get; set; }
        public int longestTaskId { get; set; }
        public int shortestTaskId { get; set; }
        public int allUsersOnProject { get; set; }
    }
}
