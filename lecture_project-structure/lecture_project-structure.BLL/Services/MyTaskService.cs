﻿using lecture_project_structure.BLL.Interfaces;
using lecture_project_structure.DAL.Entites;
using lecture_project_structure.DAL.Interfaces;
using lecture_project_structure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.Services
{
    public class MyTaskService : IMyTaskService
    {
        private IUnitOfWork _unitOfWork;

        public MyTaskService()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void CreateTask(MyTask task)
        {
            _unitOfWork.Tasks.Create(task);
        }

        public void DeleteTask(int id)
        {
            _unitOfWork.Tasks.Delete(id);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public IEnumerable<MyTask> GetAllTasks()
        {
            return _unitOfWork.Tasks.ReadAll();
        }

        public MyTask GetTask(int id)
        {
            return _unitOfWork.Tasks.Read(id);
        }

        public void UpdateTask(MyTask task)
        {
            if (task == null)
            {
                throw new ValidationException("Задача не найдена");
            }
            _unitOfWork.Tasks.Update(task);
        }
    }
}
