﻿using lecture_project_structure.BLL.Interfaces;
using lecture_project_structure.DAL.Entites;
using lecture_project_structure.DAL.Interfaces;
using lecture_project_structure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.Services
{
    public class TeamService : ITeamService
    {
        private IUnitOfWork _unitOfWork;

        public TeamService ()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void CreateTeam(Team team)
        {
            _unitOfWork.Teams.Create(team);
        }

        public void DeleteTeam(int id)
        {
            _unitOfWork.Teams.Delete(id);
        }
        public void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public IEnumerable<Team> GetAllTeams()
        {
            return _unitOfWork.Teams.ReadAll();
        }

        public Team GetTeam(int id)
        {
            return _unitOfWork.Teams.Read(id);
        }

        public IEnumerable<IGrouping<int?, User>> GetUsersFromTeam()
        {
            return QueryBuilder.GetUsersFromTeam();
        }

        public void UpdateTeam(Team team)
        {
            if (team == null)
            {
                throw new ValidationException("Команда не найдена");
            }
            _unitOfWork.Teams.Update(team);
        }
    }
}
