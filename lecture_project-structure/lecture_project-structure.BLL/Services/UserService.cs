﻿using lecture_project_structure.BLL.Interfaces;
using lecture_project_structure.DAL.Entites;
using lecture_project_structure.DAL.Interfaces;
using lecture_project_structure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.Services
{
    public class UserService : IUserService
    {
        private IUnitOfWork _unitOfWork;

        public UserService()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void CreateUser(User team)
        {
            _unitOfWork.Users.Create(team);
        }

        public void DeleteUser(int id)
        {
            _unitOfWork.Users.Delete(id);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public IEnumerable<User> GetAllUsers()
        {
            return _unitOfWork.Users.ReadAll();
        }

        public User GetUser(int id)
        {
            return _unitOfWork.Users.Read(id);
        }


        public Dictionary<string, int> GetTasksInProject(int id)
        {
            return QueryBuilder.GetTasksInProject(id);
        }

        public List<MyTask> GetTasksByUser(int id)
        {
            return QueryBuilder.GetTasksByUser(id);
        }

        public List<MyTaskInfo> GetFinishedTasksByUser(int id)
        {
            return QueryBuilder.GetFinishedTasksByUser(id);
        }

        public List<User> GetUsersByName()
        {
            return QueryBuilder.GetUsersByName();
        }

        public UserInfo GetUserInfo(int id)
        {
            return QueryBuilder.GetUserInfo(id);
        }
        public void UpdateUser(User user)
        {
            if (user == null)
            {
                throw new ValidationException("Пользователь не найден");
            }
            _unitOfWork.Users.Update(user);
        }
    }
}
