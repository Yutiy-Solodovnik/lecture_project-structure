﻿using lecture_project_structure.BLL.Interfaces;
using lecture_project_structure.DAL.Entites;
using lecture_project_structure.DAL.Interfaces;
using lecture_project_structure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lecture_project_structure.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private IUnitOfWork _unitOfWork;

        public ProjectService()
        {
            _unitOfWork = new UnitOfWork();
        }
        public void CreateProject(Project project)
        {
            _unitOfWork.Projects.Create(project);
        }

        public void DeleteProject(int id)
        {
            _unitOfWork.Projects.Delete(id);
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }

        public IEnumerable<Project> GetAllProjects()
        {
            return _unitOfWork.Projects.ReadAll();
        }

        public Project GetProject(int id)
        {
            return _unitOfWork.Projects.Read(id);
        }

        public  List<ProjectInfo> GetProjectInfo()
        {
            return QueryBuilder.GetProjectInfo();
        }

        public void UpdateProject(Project project)
        {
            if (project == null)
            {
                throw new ValidationException("Проект не найден");
            }
            _unitOfWork.Projects.Update(project);
        }
    }
}
