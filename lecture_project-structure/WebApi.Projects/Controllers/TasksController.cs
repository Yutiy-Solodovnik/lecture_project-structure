﻿using AutoMapper;
using lecture_project_structure.BLL.DTO;
using lecture_project_structure.BLL.Services;
using lecture_project_structure.DAL.Entites;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.MyTasks.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TasksController : Controller
    {
        private MyTaskService _myTaskService;
        private readonly IMapper _mapper;

        public TasksController(IMapper mapper, MyTaskService myTaskService)
        {
            _mapper = mapper;
            _myTaskService = myTaskService;
        }

        [HttpGet("{id}")]
        public ActionResult<MyTaskDTO> GetMyTask(string id)
        {
            MyTask myTask = _myTaskService.GetTask(Int32.Parse(id));
            MyTaskDTO myTaskDTO = _mapper.Map<MyTaskDTO>(myTask);
            return Ok(myTaskDTO);
        }
        
        [HttpGet]
        public ActionResult<IEnumerable<MyTaskDTO>> GetALLMyTasks()
        {
            IEnumerable<MyTask> myTasks = _myTaskService.GetAllTasks();
            IEnumerable<MyTaskDTO> myTasksDTO = _mapper.Map<IEnumerable<MyTaskDTO>>(myTasks);
            return Ok(myTasksDTO);
        }

        [HttpPost]
        public ActionResult<MyTaskDTO> CreateMyTask()
        {
            Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
            MyTaskDTO myTaskDTO = JsonConvert.DeserializeObject<MyTaskDTO>(todoJson.Result);
            MyTask myTask = _mapper.Map<MyTask>(myTaskDTO);
            _myTaskService.CreateTask(myTask);
            return Ok(myTask);
        }

        [HttpDelete("{id}")]
        public ActionResult<string> DeleteMyTask(string id)
        {
            _myTaskService.DeleteTask(Int32.Parse(id));
            return Ok("done");
        }

        [HttpPut]
        public ActionResult<string> UpdateMyTask()
        {
            Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
            MyTaskDTO myTaskDTO = JsonConvert.DeserializeObject<MyTaskDTO>(todoJson.Result);
            MyTask myTask = _mapper.Map<MyTask>(myTaskDTO);
            _myTaskService.UpdateTask(myTask);
            return Ok("done");
        }
    }
}
