﻿using AutoMapper;
using lecture_project_structure.BLL.DTO;
using lecture_project_structure.BLL.Services;
using lecture_project_structure.DAL.Entites;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Teams.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TeamsController : Controller
    {
        private TeamService _teamService;
        private readonly IMapper _mapper;

        public TeamsController(IMapper mapper, TeamService teamService)
        {
            _mapper = mapper;
            _teamService = teamService;
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> GetTeam(string id)
        {
            Team team = _teamService.GetTeam(Int32.Parse(id));
            TeamDTO teamDTO = _mapper.Map<TeamDTO>(team);
            return Ok(teamDTO);
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> GetALLTeams()
        {
            IEnumerable<Team> teams = _teamService.GetAllTeams();
            IEnumerable<TeamDTO> teamsDTO = _mapper.Map<IEnumerable<TeamDTO>>(teams);
            return Ok(teamsDTO);
        }

        [HttpGet("usersFromTeams")]
        public ActionResult<IEnumerable<IGrouping<int?, UserDTO>>> GetUsersFromTeam()
        {
            IEnumerable<IGrouping<int?, User>> users = _teamService.GetUsersFromTeam();
            return Ok(users);
        }

        [HttpPost]
        public ActionResult<TeamDTO> CreateTeam()
        {
            Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
            TeamDTO teamDTO = JsonConvert.DeserializeObject<TeamDTO>(todoJson.Result);
            Team team = _mapper.Map<Team>(teamDTO);
            _teamService.CreateTeam(team);
            return Ok(team);
        }

        [HttpDelete("{id}")]
        public ActionResult<string> DeleteTeam(string id)
        {
            _teamService.DeleteTeam(Int32.Parse(id));
            return Ok("done");
        }

        [HttpPut]
        public ActionResult<string> UpdateTeam()
        {
            Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
            TeamDTO teamDTO = JsonConvert.DeserializeObject<TeamDTO>(todoJson.Result);
            Team team = _mapper.Map<Team>(teamDTO);
            _teamService.UpdateTeam(team);
            return Ok("done");
        }
    }
}
