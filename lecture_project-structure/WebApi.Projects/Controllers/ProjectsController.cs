﻿using AutoMapper;
using lecture_project_structure.BLL.DTO;
using lecture_project_structure.BLL.Services;
using lecture_project_structure.DAL.Entites;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Projects.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class ProjectsController : ControllerBase
    {
        private ProjectService _projectService;
        private readonly IMapper _mapper;
        
        public ProjectsController(IMapper mapper, ProjectService projectService)
        {
            _mapper = mapper;
            _projectService = projectService;
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> GetProject(string id)
        {
            Project project = _projectService.GetProject(Int32.Parse(id));
            ProjectDTO projectDTO = _mapper.Map<ProjectDTO>(project);
            return Ok(projectDTO);
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> GetALLProjects()
        {
            IEnumerable<Project> projects = _projectService.GetAllProjects();
            IEnumerable<ProjectDTO> projectsDTO = _mapper.Map<IEnumerable<ProjectDTO>>(projects);
            return Ok(projectsDTO);
        }

        [HttpGet("projectInfo")]
        public ActionResult<List<UserDTO>> GetUsersByName()
        {
            List<ProjectInfo> projectInfo = _projectService.GetProjectInfo();
            List<ProjectInfoDTO> projectInfoDTO = _mapper.Map<List<ProjectInfoDTO>>(projectInfo);
            return Ok(projectInfoDTO);
        }

        [HttpPost]
        public ActionResult<ProjectDTO> CreateProject()
        {
            Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
            ProjectDTO projectDTO = JsonConvert.DeserializeObject<ProjectDTO>(todoJson.Result);
            Project project = _mapper.Map<Project>(projectDTO);
            _projectService.CreateProject(project);
            return Ok(project);
        }

        [HttpDelete("{id}")]
        public ActionResult<string> DeleteProject(string id)
        {
            _projectService.DeleteProject(Int32.Parse(id));
            return Ok("done");
        }

        [HttpPut]
        public ActionResult<string> UpdateProject()
        {
            Task<string> todoJson = new StreamReader(Request.Body).ReadToEndAsync();
            ProjectDTO projectDTO = JsonConvert.DeserializeObject<ProjectDTO>(todoJson.Result);
            Project project = _mapper.Map<Project>(projectDTO);
            _projectService.UpdateProject(project);
            return Ok("done");
        }
    }
}
