﻿using AutoMapper;
using lecture_project_structure.BLL.DTO;
using lecture_project_structure.DAL.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Projects
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>();
            CreateMap<MyTask, MyTaskDTO>();
            CreateMap<MyTaskDTO, MyTask>();
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();
            CreateMap<UserInfo, UserInfoDto>();
            CreateMap<UserInfoDto, UserInfo>();
            CreateMap<MyTaskInfo, MyTaskInfoDTO>();
            CreateMap<MyTaskInfoDTO, MyTaskInfo>();
            CreateMap<ProjectInfo, ProjectInfoDTO>();
            CreateMap<ProjectInfoDTO, ProjectInfo>();
        }
    }
}
