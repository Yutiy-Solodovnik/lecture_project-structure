using lecture_project_structure.BLL.Interfaces;
using lecture_project_structure.BLL.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Projects.Controllers;

namespace WebApi.Projects
{
    public class Startup
    {
        private ProjectService _projectService;
        private MyTaskService _taskService;
        private TeamService _teamService;
        private UserService _userService;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _projectService = new ProjectService();
            _taskService = new MyTaskService();
            _teamService = new TeamService();
            _userService = new UserService();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ProjectService>(_projectService);
            services.AddSingleton<MyTaskService>(_taskService);
            services.AddSingleton<TeamService>(_teamService);
            services.AddSingleton<UserService>(_userService);
            services.AddAutoMapper(typeof(Startup));
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebApi.Projects", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebApi.Projects v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
